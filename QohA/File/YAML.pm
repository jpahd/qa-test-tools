package QohA::File::YAML;

use Modern::Perl;
use Moo;
use YAML::XS;

extends 'QohA::File';

sub run_checks {
    my ($self, $cnt) = @_;
    my $r = $self->check_parse_yaml();
    $self->SUPER::add_to_report('yaml_valid', $r);

    if ( $self->path =~ m{^api/} ) {
        $r = $self->check_rest_api_specs();
        $self->SUPER::add_to_report('rest_api_specs', $r);
    }

    return $self->SUPER::run_checks($cnt);
}

sub check_parse_yaml {
    my ($self) = @_;
    return 0 unless -e $self->path;
    eval { YAML::XS::LoadFile($self->abspath); };
    return 0 unless $@;

    my $error = $@;
    $error =~ s|\n| |g;
    return [$error];
}

sub check_rest_api_specs {
    my ($self) = @_;
    return 0 unless -e $self->path;

    my $git_grep_cmd = sprintf q{git grep '^\s*x-koha-embed:$' %s }, $self->path;
    my @matches = `$git_grep_cmd`;
    my $error = @matches
       ? sprintf q{x-koha-embed must not be at path top level but defined as as enum, see bug 30536}
       : q{};
    return [$error];
}

1;

=head1 AUTHOR
Mason James <mtj at kohaaloha.com>
Jonathan Druart <jonathan.druart@biblibre.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2012 by KohaAloha and BibLibre

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007
=cut
